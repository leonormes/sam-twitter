// Set up express
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
// setup Twitter library to use Twitters API
const client = require('./client');
// Secret .env variables
// Express middleware
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

/**
 * Gets the tweets from the user specified in the url
 * Defaults to getting the past 10 tweets, but takes optional param
 * to change this number
 */
app.get('/tweetsfrom/:handle/:count?', (req, res) => {
  const count = isNaN(req.params.count) ? '10' : req.params.count;
  client.get(
    'search/tweets',
    {q: 'from:' + req.params.handle, count: count},
    function(err, data, response) {
      if (err) {
        res.send(err);
      } else if (!data.statuses[0]) {
        res.status(404).send('Twitter user not found');
      } else {
        const responseText = {};
        responseText.username = data.statuses[0].user.screen_name;
        responseText.tweets = [];
        data.statuses.forEach((tw) => {
          responseText.tweets.push(tw.text);
        });
        res.json(responseText);
      }
    }
  );
});

/**
 * If no params given send instructions!
 */
app.get('/', (req, res) => {
  res.send('please use the API');
});

/**
 * Post a tweet to twitter. The tweet should be in the body as JSON.
 */
app.post('/sendtweet/', jsonParser, (req, res) => {
  if (!req.body || req.body.status === undefined) {
    return res.status(400).send('No status/tweet supplied');
  } else {
    client
      .post('statuses/update', req.body)
      .then(function(tweet) {
        res.status(200).send(tweet);
      })
      .catch(function(error) {
        res.send(error);
      });
  }
});

app.listen(port, () =>
  console.log('Samlabs twitter app listening on port ' + port)
);
