# README

A solution to the SAMLabs test.

* Create a NodeJS web service, with your web tech stack of choice
  * Integrate with Twitter
    * Create a new account, if you don’t already have one
  * Create a GET endpoint that lists all the tweets of a given user (to be provided in the URL) eg, https://twitter.com/samlabs
  * Create a POST endpoint to create a new tweet. You can just use the consumer key/secret for authentication.

## How do I get set up

### Retrieve tweets

* The base URL is [https://sam-twitter.herokuapp.com/]
* To retrieve 10 tweets from a given user [https://sam-twitter.herokuapp.com/tweetsfrom/samlabs]
* Or if you want a different number of tweets [https://sam-twitter.herokuapp.com/tweetsfrom/samlabs/3]

### Post a tweet

* To post a tweet [https://sam-twitter.herokuapp.com/sendtweet]
* And in the body of type/json include `{"status":"We love twitter"}`
